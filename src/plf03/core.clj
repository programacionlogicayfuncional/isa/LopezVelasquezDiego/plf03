(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (- 15 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (+ 9 x))
        g (fn [x] (- 6 x))
        z (comp f g)]
    (z 7)))

(defn función-comp-3
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (map inc xs))
        z (comp f g)]
    (z #{-1 2 -2 3 -3 4 -4})))

(defn función-comp-4
  []
  (let [f (fn [xs] (into [] (remove neg? xs)))
        g (fn [xs] (map inc xs))
        z (comp f g)]
    (z [-12 5 4 -8 9 1 -10 -11])))

(defn función-comp-5
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (map-entry? xs))
        z (comp f g)]
    (z '(-5 9 7 -4 -8 -6 6 4))))

(defn función-comp-6
  []
  (let [f (fn [xs] (keyword xs))
        g (fn [xs] (str xs))
        z (comp f g)]
    (z [15 30])))

(defn función-comp-7
  []
  (let [f (fn [xs] (second xs))
        g (fn [xs] (reverse xs))
        z (comp f g)]
    (z '(false 15 20 "bye"))))

(defn función-comp-8
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (map dec xs))
        z (comp f g)]
    (z [49 50 30 19 9 -5 -4])))

(defn función-comp-9
  []
  (let [f (fn [x] (not x))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 1000)))

(defn función-comp-10
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (double xs))
        z (comp f g)]
    (z (+ 4 8 9 10))))

(defn función-comp-11
  []
  (let [f (fn [x] (char? x))
        g (fn [x] (first x))
        z (comp f g)]
    (z "studio")))

(defn función-comp-12
  []
  (let [f (fn [xs] (ident? xs))
        g (fn [xs] (empty xs))
        z (comp f g)]
    (z [\a \A \b \B \c \C])))

(defn función-comp-13
  []
  (let [f (fn [xs] (indexed? xs))
        g (fn [xs] (list xs))
        z (comp f g)]
    (z {true 1 2 "hi" \V \b})))

(defn función-comp-14
  []
  (let [f (fn [xs] (int? xs))
        g (fn [xs] (first xs))
        z (comp f g)]
    (z [1 2 3 4 5 6])))

(defn función-comp-15
  []
  (let [f (fn [xs] (+ 4 xs))
        g (fn [xs] (* 8 xs))
        z (comp f g)]
    (z 25)))

(defn función-comp-16
  []
  (let [f (fn [xs] (filter even? xs))
        g (fn [xs] (map inc xs))
        z (comp f g)]
    (z #{1 3 5 2 6 4 7})))

(defn función-comp-17
  []
  (let [f (fn [x] (+ 72 x))
        g (fn [x] (- 18 x))
        z (comp f g)]
    (z 7)))

(defn función-comp-18
  []
  (let [f (fn [x] (- 4 x))
        g (fn [x] (- 9 x))
        h (fn [x] (* 7 x))
        z (comp f g h)]
    (z 15)))

(defn función-comp-19
  []
  (let [f (fn [x] (* 5 x))
        g (fn [x] (* 10 x))
        z (comp f g)]
    (z 15)))

(defn función-comp-20
  []
  (let [f (fn [xs] (filter neg? xs))
        g (fn [xs] (map dec xs))
        z (comp f g)]
    (z #{-1 2 -2 3 -3 4 -4})))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complemet-1
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 15)))

(defn función-complemet-2
  []
  (let [f (fn [xs] (empty? xs))
        z (complement f)]
    (z [-9 -5 -4])))

(defn función-complemet-3
  []
  (let [f (fn [xs] (list? xs))
        z (complement f)]
    (z '(1 2 3 4))))

(defn función-complemet-4
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z :hi)))

(defn función-complemet-5
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z 8)))

(defn función-complemet-6
  []
  (let [f (fn [xs] (vector? xs))
        z (complement f)]
    (z [1 5 7 9])))

(defn función-complemet-7
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z 45)))

(defn función-complemet-8
  []
  (let [f (fn [xs] (coll? xs))
        z (complement f)]
    (z #{5 6 7 4})))

(defn función-complemet-9
  []
  (let [f (fn [x] (decimal? x))
        z (complement f)]
    (z 1.0)))

(defn función-complemet-10
  []
  (let [f (fn [xs] (map? xs))
        z (complement f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-complemet-11
  []
  (let [f (fn [xs] (indexed? xs))
        z (complement f)]
    (z #{25 4 5 6 })))

(defn función-complemet-12
  []
  (let [f (fn [x] (neg? x))
        z (complement f)]
    (z -648)))

(defn función-complemet-13
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z 1)))

(defn función-complemet-14
  []
  (let [f (fn [x] (ratio? x))
        z (complement f)]
    (z 22/7)))

(defn función-complemet-15
  []
  (let [f (fn [x] (seqable? x))
        z (complement f)]
    (z "clojure")))

(defn función-complemet-16
  []
  (let [f (fn [xs] (set? xs))
        z (complement f)]
    (z #{1 2 3})))

(defn función-complemet-17
  []
  (let [f (fn [x] (some? x))
        z (complement f)]
    (z 42)))

(defn función-complemet-18
  []
  (let [f (fn [x] (string? x))
        z (complement f)]
    (z \a)))
  
(defn función-complemet-19
  []
  (let [f (fn [x] (symbol? x))
        z (complement f)]
    (z 'a)))

(defn función-complemet-20
  []
  (let [f (fn [xs] (associative? xs))
        z (complement f)]
    (z [1 2 3])))

(función-complemet-1)
(función-complemet-2)
(función-complemet-3)
(función-complemet-4)
(función-complemet-5)
(función-complemet-6)
(función-complemet-7)
(función-complemet-8)
(función-complemet-9)
(función-complemet-10)
(función-complemet-11)
(función-complemet-12)
(función-complemet-13)
(función-complemet-14)
(función-complemet-15)
(función-complemet-16)
(función-complemet-17)
(función-complemet-18)
(función-complemet-19)
(función-complemet-20)

(defn función-constantly-1
  []
  (let [f #{1 2 3}
        z (constantly f)]
    (z 50)))

(defn función-constantly-2
  []
  (let [f true
        z (constantly f)]
    (z #{\q \r \y})))

(defn función-constantly-3
  []
  (let [f [true false "clojure"]
        z (constantly f)]
    (z {:a 1 :b 2 :c 3})))

(defn función-constantly-4
  []
  (let [f "miercoles"
        z (constantly f)]
    (z '(11 5 4))))

(defn función-constantly-5
  []
  (let [f '(\a \b \c d)
           z (constantly f)]
    (z \a 10 "hi")))

(defn función-constantly-6
  []
  (let [f false 
        z (constantly f)]
    (z 15 20 25)))

(defn función-constantly-7
  []
  (let [f [:a :b :c]
        z (constantly f)]
    (z #{\q \r \y})))

(defn función-constantly-8
  []
  (let [f [1 2 3 4 5]
        z (constantly f)]
    (z {"hi" "hola"})))

(defn función-constantly-9
  []
  (let [f 1500 
           z (constantly f)]
    (z {:a 1 :b 2})))

(defn función-constantly-10
  []
  (let [f [\a 1 "clojure" :a]
           z (constantly f)]
    (z true 2.0 "hi")))

(defn función-constantly-11
  []
  (let [f [true false false true true]
        z (constantly f)]
    (z true 2.0)))

(defn función-constantly-12
  []
  (let [f 12.15
        z (constantly f)]
    (z {4 4 2 5})))

(defn función-constantly-13
  []
  (let [f 9
        z (constantly f)]
    (z [1 2 3])))

(defn función-constantly-14
  []
  (let [f \A
        z (constantly f)]
    (z {:a 1})))

(defn función-constantly-15
  []
  (let [f #{"clojure" "hi"}
        z (constantly f)]
    (z {:a 1})))

(defn función-constantly-16
  []
  (let [f 40
        z (constantly f)]
    (z 40 60 80)))

(defn función-constantly-17
  []
  (let [f {:a 1 :b 2 :c 3 :d 4}
        z (constantly f)]
    (z 40)))

(defn función-constantly-18
  []
  (let [f nil
        z (constantly f)]
    (z \a)))

(defn función-constantly-19
  []
  (let [f #{"visual" "studio" "code"}
        z (constantly f)]
    (z "clojure")))

(defn función-constantly-20
  []
  (let [f [10 20 30 40 50 60]
           z (constantly f)]
    (z true false false true)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (odd? x))
        z (every-pred f g)]
    (z 15)))

(defn función-every-pred-2
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (coll? xs))
        z (every-pred f g)]
    (z [1 2 3 4])))

(defn función-every-pred-3
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (number? x))
        z (every-pred f g)]
    (z 45)))

(defn función-every-pred-4
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (coll? x))
        z (every-pred f g)]
    (z {:a 1 :b 2 :c 3})))

(defn función-every-pred-5
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z 15.0)))

(defn función-every-pred-6
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (associative? x))
        h (fn [x] (coll? x))
        z (every-pred f g h)]
    (z '(1 2 3 4 5))))

(defn función-every-pred-7
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (zero? x))
        h (fn [x] (double? x))
        i (fn [x] (int? x))
        z (every-pred f g h i)]
    (z 1)))


(defn función-every-pred-8
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (ratio? x))
        z (every-pred f g)]
    (z '(22/7 55/4))))

(defn función-every-pred-9
  []
  (let [f (fn [x] (set? x))
        g (fn [x] (indexed? x))
        z (every-pred f g)]
    (z #{10 20 30 40})))

(defn función-every-pred-10
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (double? x))
        z (every-pred f g)]
    (z -12.5)))

(defn función-every-pred-11
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (true? x))
        h (fn [x] (some? x))
        z (every-pred f g h)]
    (z true)))

(defn función-every-pred-12
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (char? x))
        z (every-pred f g)]
    (z {\a \A \b \B})))

(defn función-every-pred-13
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (int? x))
        z (every-pred f g)]
    (z 42)))

(defn función-every-pred-14
  []
  (let [f (fn [xs] (map even? xs))
        g (fn [xs] (map boolean? xs))
        h (fn [xs] (map true? xs))
        z (every-pred f g h)]
    (z [1 2 3 4 5 6 7 8 9])))

(defn función-every-pred-15
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (integer? x))
        z (every-pred f g)]
    (z 15)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (rational? x))
        z (every-pred f g)]
    (z 62)))

(defn función-every-pred-17
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (map? xs))
        z (every-pred f g)]
    (z {1 2 3 4 5 6})))

(defn función-every-pred-18
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (char? x))
        z (every-pred f g)]
    (z 'x)))

(defn función-every-pred-19
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (int? x))
        z (every-pred f g)]
    (z [1 2 3 4])))

(defn función-every-pred-20
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (indexed? x))
        z (every-pred f g)]
    (z [23 2 5 1 5])))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn [x] (dec x))
        z (fnil f nil)]
    (z 1)))

(defn función-fnil-2
  []
  (let [f (fn [x] (inc x))
        z (fnil f nil)]
    (z 15)))

(defn función-fnil-3
  []
  (let [f (fn [x y] (if (< x y) (/ x y) (* x y)))
        z (fnil f nil nil)]
    (z 7 6)))

(defn función-fnil-4
  []
  (let [f (fn [xs ys] (conj [] xs ys))
        z (fnil f nil nil)]
    (z #{10 20} #{30 40})))

(defn función-fnil-5
  []
  (let [f (fn [a b c] (* a b c))
        z (fnil f nil nil nil)]
    (z 4 5 6)))

(defn función-fnil-6
  []
  (let [f (fn [xs] (keys xs))
        z (fnil f nil)]
    (z {:a 1 :b 2 :c 3 :d 4})))

(defn función-fnil-7
  []
  (let [f (fn [x] (dec (inc (dec x))))
        z (fnil f nil)]
    (z 15)))

(defn función-fnil-8
  []
  (let [f (fn [xs x] (contains? xs x))
        z (fnil f nil nil)]
    (z {:a 1 :b 2 :c 3 :d 4} :b)))

(defn función-fnil-9
  []
  (let [f (fn [xs y w] (conj xs y w))
        z (fnil f nil nil nil)]
    (z [10 15 20 25 30] 35 40)))

(defn función-fnil-10
  []
  (let [f (fn [w x y] (if (> w y) (* w x y) (/ w x y)))
        z (fnil f nil nil nil)]
    (z 10 8 6)))

(defn función-fnil-11
  []
  (let [f (fn [xs ys] (count (into xs ys)))
        z (fnil f nil nil)]
    (z [3 6 9 12] [15 18 21 24])))

(defn función-fnil-12
  []
  (let [f (fn [xs] (map char xs))
        z (fnil f nil)]
    (z (range 65 91))))

(defn función-fnil-13
  []
  (let [f (fn [xs ys] (conj [] (map char xs) (map char ys)))
        z (fnil f nil nil)]
    (z (range 65 91) (range 97 123) )))

(defn función-fnil-14
  []
  (let [f (fn [xs x] (and (<= x (first xs)) (>= x (last xs))))
        z (fnil f nil nil)]
    (z #{1 5 4 13 20 15 8} 10)))

(defn función-fnil-15
  []
  (let [f (fn [x] (string? x))
        z (fnil f nil)]
    (z "clojure")))


(defn función-fnil-16
  []
  (let [f (fn [x y] (* x y))
        z (fnil f nil nil)]
    (z 10 40)))

(defn función-fnil-17
  []
  (let [f (fn [xs ys] (map + xs ys))
        z (fnil f nil nil)]
    (z [2 4 6 8] [3 5 7 9])))

(defn función-fnil-18
  []
  (let [f (fn [x] (keyword? x))
        z (fnil f nil)]
    (z :A)))

(defn función-fnil-19
  []
  (let [f (fn [xs] (into [] (remove neg? xs)))
        z (fnil f nil)]
    (z [1 -5 4 9 -7 -2 -6])))

(defn función-fnil-20
  []
  (let [f (fn [xs ys] (filter pos? (map - xs ys)))
        z (fnil f nil nil)]
    (z [-1 2 -3 4] [6 4 -7 -9])))

(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

(defn función-juxt-1
  []
  (let [f (fn [x] (count x))
        z (juxt f)]
    (z "visual studio code")))

(defn función-juxt-2
  []
  (let [f (fn [x] (first x))
        z (juxt f)]
    (z "clojure")))

(defn función-juxt-3
  []
  (let [f (fn [xs] (not xs))
        g (fn [xs] (filterv false? xs))
        z (juxt f g)]
    (z '(true false))))

(defn función-juxt-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (int? x))
        z (juxt f g)]
    (z 25)))

(defn función-juxt-5
  []
  (let [f (fn [xs] (drop 5 xs))
        g (fn [xs] (group-by even? xs))
        z (juxt g f)]
    (z (range 5 15))))

(defn función-juxt-6
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map dec xs))
        z (juxt g f)]
    (z [1 5 4 9 12 20 35])))

(defn función-juxt-7
  []
  (let [f (fn [xs] (map inc xs))
        g (fn [xs] (take (last xs) xs))
        h (fn [xs] (into #{} xs))
        z (juxt f g h)]
    (z [1 4 5 6 8 7 5 2 1 10 21 15])))

(defn función-juxt-8
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (associative? xs))
        h (fn [xs] (map? xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6 7 8 9 10 ])))

(defn función-juxt-9
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (list? xs))
        z (juxt f g)]
    (z [])))

(defn función-juxt-10
  []
  (let [f (fn [xs] (filter odd? xs))
        g (fn [xs] (map dec xs))
        z (juxt g f)]
    (z [9 6 3 2 7 1 4 5])))

(defn función-juxt-11
  []
  (let [f (fn [x] (int x))
        g (fn [x] (boolean? x))
        z (juxt f g)]
    (z 35)))

(defn función-juxt-12
  []
  (let [f (fn [x] (even? x))
        g (fn [x] (decimal? x))
        z (juxt f g)]
    (z 100)))

(defn función-juxt-13
  []
  (let [f (fn [x] (integer? x))
        g (fn [x] (pos? x))
        z (juxt f g)]
    (z 2.0)))

(defn función-juxt-14
  []
  (let [f (fn [x] (dec (inc (dec (inc x)))))
        g (fn [x] (* 10 x))
        h (fn [x] (/ x 2))
        z (juxt f g h)]
    (z 10)))

(defn función-juxt-15
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (dec x))
        h (fn [x] (* x x))
        z (juxt g h f)]
    (z 15)))

(defn función-juxt-16
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (int? x))
        h (fn [x] (ratio? x))
        z (juxt f g h)]
    (z 22/7)))

(defn función-juxt-17
  []
  (let [f (fn [xs] (into [] (remove pos? xs)))
        g (fn [xs] (reverse xs))
        h (fn [xs] (map inc xs))
        z (juxt f g h)]
    (z [1 2 4 7 -4 -5 5 -6])))

(defn función-juxt-18
  []
  (let [f (fn [xs] (list? xs))
        z (juxt f)]
    (z '(1 2 3 4 5))))

(defn función-juxt-19
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (zero? x))
        h (fn [x] (double? x))
        i (fn [x] (int? x))
        z (juxt f g h i)]
    (z 15)))

(defn función-juxt-20
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (/ x x))
        h (fn [x] (* x x))
        i (fn [x] (- x x))
        z (juxt h f i g)]
    (z 3)))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

(defn función-partial-1
  []
  (let [f (fn [x] (inc x))
        z (partial f)]
    (z 15)))

(defn función-partial-2
  []
  (let [f (fn [xs ys] (vector xs ys))
        z (partial  f 7)]
    (z 9)))

(defn función-partial-3
  []
  (let [f (fn [v w x y] (* v w x y))
        z (partial f 4 8 9)]
    (z 5)))

(defn función-partial-4
  []
  (let [f (fn [xs ys] (range xs ys))
        z (partial f 1)]
    (z 9)))

(defn función-partial-5
  []
  (let [f (fn [w x y] (hash-set w x y))
        z (partial f)]
    (z 10 20 30)))

(defn función-partial-6
  []
  (let [f (fn [x y] (if (< x y) (* x y) (/ x y)))
        z (partial f 100)]
    (z 200)))

(defn función-partial-7
  []
  (let [f (fn [xs ys] (drop xs ys))
        z (partial f 20)]
    (z [10 15 20 25 30])))

(defn función-partial-8
  []
  (let [f (fn [xs ys] (drop-last xs ys))
        z (partial f 1)]
    (z [100 200 300 400])))

(defn función-partial-9
  []
  (let [f (fn [xs x] (contains? xs x))
        z (partial f {:a 1 :b 2 :c 3})]
    (z :a)))

(defn función-partial-10
  []
  (let [f (fn [xs ys ws] (conj [] xs ys ws))
        z (partial f [1 2 3])]
    (z [4 5 6] [7 8 9])))

(defn función-partial-11
  []
  (let [f (fn [xs y] (hash-map xs y))
        z (partial f 5)]
    (z #{1 2 3 4})))

(defn función-partial-12
  []
  (let [f (fn [x y] (+ x y))
        z (partial f 10)]
    (z 15)))

(defn función-partial-13
  []
  (let [f (fn [xs ys] (into xs ys))
        z (partial f [])]
    (z [4 8])))

(defn función-partial-14
  []
  (let [f (fn [xs] (map char xs))
        z (partial f)]
    (z (range 97 123))))

(defn función-partial-15
  []
  (let [f (fn [w x y] (if (> w y) (+ w x y) (- w x y)))
        z (partial f 5)]
    (z 12 8)))

(defn función-partial-16
  []
  (let [f (fn [xs] (keys xs))
        z (partial f {:a 1 :b 2 :c 3 :d 4})]
    (z)))

(defn función-partial-17
  []
  (let [f (fn [xs] (vals xs))
        z (partial f)]
    (z {:a 1 :b 2 :c 3 :d 4})))

(defn función-partial-18
  []
  (let [f (fn [v w x y] (if (< w y) (* v w x y) (/ v w x y)))
        z (partial f 25 15)]
    (z 4 6)))

(defn función-partial-19
  []
  (let [f (fn [ws xs ys] (concat ws xs ys))
        z (partial f #{10 20 30})]
    (z [40 50 60] '(70 80 90))))

(defn función-partial-20
  []
  (let [f (fn [xs x] (and (>= x (first xs)) (<= x (last xs))))
        z (partial f #{10 20 30 40 50 60 70 80 })]
    (z 10)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [f (fn [x] (even? x))
        z (some-fn f)]
    (z 8)))

(defn función-some-fn-2
  []
  (let [f (fn [x] (keyword? x))
        g (fn [x] (char? x))
        z (some-fn g f)]
    (z \a)))

(defn función-some-fn-3
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (pos? x))
        h (fn [x] (even? x))
        z (some-fn f g h)]
    (z 10)))

(defn función-some-fn-4
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (zero? x))
        h (fn [x] (double? x))
        i (fn [x] (int? x))
        z (some-fn f g h i)]
    (z 10)))

(defn función-some-fn-5
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (true? x))
        h (fn [x] (some? x))
        z (some-fn f g h)]
    (z false)))

(defn función-some-fn-6
  []
  (let [f (fn [xs] (seq? (seq xs)))
        g (fn [xs] (vector? xs))
        z (some-fn f g)]
    (z [1 2 3 4 5])))

(defn función-some-fn-7
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (ratio? x))
        z (some-fn f g )]
    (z 48/52)))

(defn función-some-fn-8
  []
  (let [f (fn [xs] (sorted? (sorted-set xs)))
        z (some-fn f)]
    (z [1 4 8 6 5 9])))

(defn función-some-fn-9
  []
  (let [f (fn [x] (pos? x))
        z (some-fn f)]
    (z 50)))

(defn función-some-fn-10
  []
  (let [f (fn [x] (string? x))
        g (fn [x] (char? x))
        z (some-fn f g)]
    (z "clojure")))

(defn función-some-fn-11
  []
  (let [f (fn [xs] (map number? xs))
        z (some-fn f)]
    (z [1 4 2 3 5 6 8])))

(defn función-some-fn-12
  []
  (let [f (fn [xs] (filter pos? xs))
        g (fn [xs] (map int? xs))
        z (some-fn g f)]
    (z [-1 1 -2 2 3 -3 4 -4 5 -5 6 -6 7 -7 8 -8 9 -9])))

(defn función-some-fn-13
  []
  (let [f (fn [xs] (map char? (map char xs)))
        z (some-fn f)]
    (z (range 97 123))))

(defn función-some-fn-14
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (int? x))
        z (some-fn f g)]
    (z -59)))

(defn función-some-fn-15
  []
  (let [f (fn [xs] (empty? xs))
        g (fn [xs] (vector? xs))
        z (some-fn f g)]
    (z [1 2 3])))

(defn función-some-fn-16
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (associative? xs))
        h (fn [xs] (vector? xs))
        z (some-fn f g h)]
    (z [1 2 3 4 5 6 7 8 9 10])))

(defn función-some-fn-17
  []
  (let [f (fn [xs] (map neg-int? xs))
        z (some-fn f)]
    (z '(-8 5 4 -1 2 3 -8 4 9 -3 -8 -7 1 -3 -8 8 9))))

(defn función-some-fn-18
  []
  (let [f (fn [xs] (list? xs))
        g (fn [xs] (sequential? xs))
        z (some-fn f g)]
    (z '(1 2 3 4 5 6 7 8 9 10))))

(defn función-some-fn-19
  []
  (let [f (fn [xs] (map even? xs))
        g (fn [xs] (map boolean? xs))
        h (fn [xs] (map true? xs))
        z (some-fn f g h)]
    (z [1 2 3 4 5 6])))

(defn función-some-fn-20
  []
  (let [f (fn [xs] (associative? xs))
        g (fn [xs] (map? xs))
        h (fn [xs] (keyword? (key (first xs))))
        i (fn [xs x] (contains? xs x))
        z (some-fn f g h i)]
    (z {:a 1 :b 2 :c 3 :d 4 } :a)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)